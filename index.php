<?php

require_once 'animal.php';
require_once 'ape.php';
require_once 'frog.php';

$sheep = new animal("shaun");

echo $sheep->name;
echo $sheep->get_legs();
echo $sheep->get_cold_blooded();


$sungokong = new ape("kera sakti");
$sungokong->yell();

$kodok = new Frog("buduk");
$kodok->jump();

?>