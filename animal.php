<?php

class animal
{
    public $name;
    public $legs = 2;
    public $cold_blooded = "false";

    public function __construct($name)
    {
        $this->name = $name;
        echo $this->name;
        echo "<br>";
    }

    public function get_legs(){
        echo $this->legs;
        echo "<br>";
    }

    public function get_cold_blooded(){
        echo $this->cold_blooded;
        echo "<br>";
    }
}
